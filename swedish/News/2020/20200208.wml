#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920"
<define-tag pagetitle>Uppdaterad Debian 10; 10.3 utgiven</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin tredje uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>Dom som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på dom vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction alot "Remove expiration time from test suite keys, fixing build failure">
<correction atril "Fix segfault when no document is loaded; fix read of uninitialised memory [CVE-2019-11459]">
<correction base-files "Update for the point release">
<correction beagle "Provide wrapper script instead of symlinks to JARs, making them work again">
<correction bgpdump "Fix segmentation fault">
<correction boost1.67 "Fix undefined behaviour leading to crashing libboost-numpy">
<correction brightd "Actually compare the value read out of /sys/class/power_supply/AC/online with <q>0</q>">
<correction casacore-data-jplde "Include tables up to 2040">
<correction clamav "New upstream release; fix denial of service issue [CVE-2019-15961]; remove ScanOnAccess option, replacing with clamonacc">
<correction compactheader "New upstream release compatible with Thunderbird 68">
<correction console-common "Fix regression that led to files not being included">
<correction csh "Fix segfault on eval">
<correction cups "Fix memory leak in ppdOpen; fix validation of default language in ippSetValuetag [CVE-2019-2228]">
<correction cyrus-imapd "Add BACKUP type to cyrus-upgrade-db, fixing upgrade issues">
<correction debian-edu-config "Keep proxy settings on client if WPAD is unreachable">
<correction debian-installer "Rebuild against proposed-updates; tweak mini.iso generation on arm so EFI netboot will work; update USE_UDEBS_FROM default from unstable to buster, to help users performing local builds">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "Update security support status of several packages">
<correction debos "Rebuild against updated golang-github-go-debos-fakemachine">
<correction dispmua "New upstream release compatible with Thunderbird 68">
<correction dkimpy "New upstream stable release">
<correction dkimpy-milter "Fix privilege management at startup so Unix sockets work">
<correction dpdk "New upstream stable release">
<correction e2fsprogs "Fix potential stack underflow in e2fsck [CVE-2019-5188]; fix use after free in e2fsck">
<correction fig2dev "Allow Fig v2 text strings ending with multiple ^A [CVE-2019-19555]; reject huge arrow types causing integer overflow [CVE-2019-19746]; fix several crashes [CVE-2019-19797]">
<correction freerdp2 "Fix realloc return handling [CVE-2019-17177]">
<correction freetds "tds: Make sure UDT has varint set to 8 [CVE-2019-13508]">
<correction git-lfs "Fix build issues with newer Go versions">
<correction gnubg "Increase the size of static buffers used to build messages during program start so that the Spanish translation doesn't overflow a buffer">
<correction gnutls28 "Fix interop problems with gnutls 2.x; fix parsing of certificates using RegisteredID">
<correction gtk2-engines-murrine "Fix co-installability with other themes">
<correction guile-2.2 "Fix build failure">
<correction libburn "Fix <q>cdrskin multi-track burning was slow and stalled after track 1</q>">
<correction libcgns "Fix build failure on ppc64el">
<correction libimobiledevice "Properly handle partial SSL writes">
<correction libmatroska "Increase shared library dependency to 1.4.7 since that version introduced new symbols">
<correction libmysofa "Security fixes [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl "Fix interpretation of years from 2020 onwards">
<correction libparse-win32registry-perl "Fix interpretation of years from 2020 onwards">
<correction libperl4-corelibs-perl "Fix interpretation of years from 2020 onwards">
<correction libsolv "Fix heap buffer overflow [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "Fix previously unusable OpenDocument spreadsheets and passing of JSON formatting options">
<correction libtimedate-perl "Fix interpretation of years from 2020 onwards">
<correction libvirt "Apparmor: Allow one to run pygrub; don't render osxsave, ospke into QEMU command line; this helps newer QEMU with some configs generated by virt-install">
<correction libvncserver "RFBserver: don't leak stack memory to the remote [CVE-2019-15681]; resolve a freeze during connection closure and a segmentation fault on multi-threaded VNC servers; fix issue connecting to VMWare servers; fix crashing of x11vnc when vncviewer connects">
<correction limnoria "Fix remote information disclosure and possibly remote code execution in the Math plugin [CVE-2019-19010]">
<correction linux "New upstream stable release">
<correction linux-latest "Update for 4.19.0-8 Linux kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction mariadb-10.3 "New upstream stable release [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "Call shmget() with permission 0600 instead of 0777 [CVE-2019-5068]">
<correction mnemosyne "Add missing dependency on PIL">
<correction modsecurity "Fix cookie header parsing bug [CVE-2019-19886]">
<correction node-handlebars "Disallow calling <q>helperMissing</q> and <q>blockHelperMissing</q> directly [CVE-2019-19919]">
<correction node-kind-of "Fix type checking vulnerability in ctorName() [CVE-2019-20149]">
<correction ntpsec "Fix slow DNS retries; fix ntpdate -s (syslog) to fix the if-up hook; documentation fixes">
<correction numix-gtk-theme "Fix co-installability with other themes">
<correction nvidia-graphics-drivers-legacy-340xx "New upstream stable release">
<correction nyancat "Rebuild in a clean environment to add the systemd unit for nyancat-server">
<correction openjpeg2 "Fix heap overflow [CVE-2018-21010] and integer overflow [CVE-2018-20847]">
<correction opensmtpd "Warn users of change of smtpd.conf syntax (in earlier versions); install smtpctl setgid opensmtpq; handle non-zero exit code from hostname during config phase">
<correction openssh "Deny (non-fatally) ipc in the seccomp sandbox, fixing failures with OpenSSL 1.1.1d and Linux &lt; 3.19 on some architectures">
<correction php-horde "Fix stored cross-site scripting issue in Horde Cloud Block [CVE-2019-12095]">
<correction php-horde-text-filter "Fix invalid regular expressions">
<correction postfix "New upstream stable release">
<correction postgresql-11 "New upstream stable release">
<correction print-manager "Fix crash if CUPS returns the same ID for multiple print jobs">
<correction proftpd-dfsg "Fix CRL issues [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "Fix path to fonts">
<correction python-evtx "Fix import of <q>hexdump</q>">
<correction python-internetarchive "Close file after getting hash, avoiding file descriptor exhaustion">
<correction python3.7 "Security fixes [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "Add support for non-PPD printers and avoid silent fallback to a printer supporting PPD; fix crash when using QLabels with rich text; fix graphics tablet hover events">
<correction qtwebengine-opensource-src "Fix PDF parsing; disable executable stack">
<correction quassel "Fix quasselcore AppArmor denials when the config is saved; correct default channel for Debian; remove unnecessary NEWS file">
<correction qwinff "Fix crash due to incorrect file detection">
<correction raspi3-firmware "Fix detection of serial console with kernel 5.x">
<correction ros-ros-comm "Fix security issues [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "New upstream stable release; fix insecure permissions in enigma plugin [CVE-2018-1000071]">
<correction schleuder "Fix recognizing keywords in mails with <q>protected headers</q> and empty subject; strip non-self-signatures when refreshing or fetching keys; error if the argument provided to `refresh_keys` is not an existing list; add missing List-Id header to notification mails sent to admins; handle decryption problems gracefully; default to ASCII-8BIT encoding">
<correction simplesamlphp "Fix incompatibility with PHP 7.3">
<correction sogo-connector "New upstream release compatible with Thunderbird 68">
<correction spf-engine "Fix privilege management at startup so Unix sockets work; update documentation for TestOnly">
<correction sudo "Fix a (non-exploitable in buster) buffer overflow when pwfeedback is enabled and input is a not a tty [CVE-2019-18634]">
<correction systemd "Set fs.file-max sysctl to LONG_MAX rather than ULONG_MAX; change ownership/mode of the execution directories also for static users, ensuring that execution directories like CacheDirectory and StateDirectory are properly chowned to the user specified in User= before launching the service">
<correction tifffile "Fix wrapper script">
<correction tigervnc "Security fixes [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Security fixes [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "Fix paths to ip(6)tables-restore in light of the migration to nftables">
<correction unhide "Fix stack exhaustion">
<correction x2goclient "Strip ~/, ~user{,/}, ${HOME}{,/} and $HOME{,/} from destination paths in SCP mode; fixes regression with newer libssh versions with fixes for CVE-2019-14889 applied">
<correction xmltooling "Fix race condition that could lead to crash under load">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction caml-crush "[armel] Unbuildable due to lack of ocaml-native-compilers">
<correction firetray "Incompatible with current Thunderbird versions">
<correction koji "Security issues">
<correction python-lamson "Broken by changes in python-daemon">
<correction radare2 "Security issues; upstream do not offer stable support">
<correction radare2-cutter "Depends on to-be-removed radare2">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


