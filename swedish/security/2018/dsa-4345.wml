#use wml::debian::translation-check translation="1d1f8d159bd57a26b5a8603a6dfc4a1937981b1c" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i Samba, en SMB/CIFS fil-, utskrifts-,
och inloggningsserver för Unix. Projektet Common Vulnerabilities and
Exposures identifierar följande problem:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

	<p>Florian Stuelpner upptäckte att Samba är sårbar för
	oändlig förfrågerekursion som orsakas av CNAME-loopar, vilket
	resulterar i överbelastning.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-14629.html">\
    https://www.samba.org/samba/security/CVE-2018-14629.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16841">CVE-2018-16841</a>

	<p>Alex MacCuish upptäckte att en användare med ett giltigt certifikat
	eller smartkort kan krascha Samvba AD DC's KDC när den är konfigurerad
	att acceptera smart-kort autentisering.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16841.html">\
    https://www.samba.org/samba/security/CVE-2018-16841.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

	<p>Garming Sam från Samba-gruppen och Catalyst upptäckte en
	NULL-pekardereferenssårbarhet i Samba AD DC LDAP-servern som tillåter
	en användare som kan läsa mer än 256MB LDAP-inlägg att krascha
	Samba AD DC's LDAP-server.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16851.html">\
    https://www.samba.org/samba/security/CVE-2018-16851.html</a></p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 2:4.5.12+dfsg-2+deb9u4.</p>

<p>Vi rekommenderar att ni uppgraderar era samba-paket.</p>

<p>För detaljerad säkerhetsstatus om samba vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4345.data"
