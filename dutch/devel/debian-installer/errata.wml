#use wml::debian::template title="Het installatieprogramma van Debian - errata"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="5513b0df9f9525c15c9a757a14ac534e8d3ac03e"

<h1>Errata voor <humanversion /></h1>

<p>
Dit is een lijst van gekende problemen in de <humanversion />-release van het
installatieprogramma van Debian. Indien u uw probleem hierin niet vermeld
vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installatierapport</a>
waarin u het probleem beschrijft.
</p>

<dl class="gloss">
     <dt>Bij sommige virtuele-machineopstellingen is het mogelijk dat GNOME
     niet start</dt>
     <dd>Tijdens het testen van het image van Stretch Alpha 4 werd vastgesteld
      dat GNOME mogelijk niet start, afhankelijk van de voor virtuele machines
      gebruikte instellingen. Het lijkt erop dat het gebruik van cirrus als een
      geëmuleerde videochip prima is.
     <br />
     <b>Toestand:</b> wordt onderzocht.</dd>

     <dt>Installaties van een grafische werkomgeving kunnen mislukken wanneer
     men enkel met cd#1 installeert</dt>
     <dd>Door de beperkte ruimte op de eerste cd, passen niet alle verwachte
      GNOME-desktoppakketten op cd#1. Voor een succesvolle installatie moet u
      extra pakketbronnen (bijv. een tweede cd of een
      netwerkspiegelserver) of eerder een dvd gebruiken. <br />
      <b>Toestand:</b> Het is onwaarschijnlijk dat met meer inspanningen meer
      pakketten in te passen zijn op cd#1. </dd>

     <dt>Het voor het installatieprogramma gebruikt thema</dt>
     <dd>Er is nog geen grafisch ontwerp voor Bullseye en het
      installatieprogramma gebruikt nog altijd het Buster-thema.</dd>

     <dt>LUKS2 is niet compatibel met GRUB's cryptodisk-ondersteuning</dt>
     <dd>Pas onlangs werd vastgesteld dat GRUB geen ondersteuning biedt voor
      LUKS2, Dit betekent dat gebruikers die <tt>GRUB_ENABLE_CRYPTODISK</tt>
      willen gebruiken en een aparte niet-geëncrypteerde <tt>/boot</tt> willen
      gebruiken, dit niet zullen kunnen doen
      (<a href="https://bugs.debian.org/927165">#927165</a>). Deze opstelling
      wordt sowieso toch niet ondersteund in het installatieprogramma, maar
      het zou logisch zijn om deze beperking op zijn minst prominenter te
      documenteren en om op zijn minst de mogelijkheid te hebben om voor LUKS1
      te kiezen tijdens het installatieproces.
     <br />
     <b>Toestand:</b> Er zijn enkele ideeën geuit over de bug; de ontwikkelaars
      van cryptsetup schreven wat
      <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">specifieke documentatie</a>.</dd>


<!-- things should be better starting with Jessie Beta 2...
	<dt>GNU/kFreeBSD support</dt>

	<dd>GNU/kFreeBSD installation images suffer from various
	important bugs
	(<a href="https://bugs.debian.org/757985"><a href="https://bugs.debian.org/757985">#757985</a></a>,
	<a href="https://bugs.debian.org/757986"><a href="https://bugs.debian.org/757986">#757986</a></a>,
	<a href="https://bugs.debian.org/757987"><a href="https://bugs.debian.org/757987">#757987</a></a>,
	<a href="https://bugs.debian.org/757988"><a href="https://bugs.debian.org/757988">#757988</a></a>). Porters
	could surely use some helping hands to bring the installer back
	into shape!</dd>
-->

<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce...
	<dt>Accessibility of the installed system</dt>
	<dd>Even if accessibility technologies are used during the
	installation process, they might not be automatically enabled
	within the installed system.
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>Desktop installations on i386 do not work using CD#1 alone</dt>
	<dd>Due to space constraints on the first CD, not all of the expected GNOME desktop
	packages fit on CD#1. For a successful installation, use extra package sources (e.g.
	a second CD or a network mirror) or use a DVD instead.
	<br />
	<b>Status:</b> It is unlikely more efforts can be made to fit more packages on CD#1.
	</dd>
-->

<!-- ditto...
	<dt>Potential issues with UEFI booting on amd64</dt>
	<dd>There have been some reports of issues booting the Debian Installer in UEFI mode
	on amd64 systems. Some systems apparently do not boot reliably using <code>grub-efi</code>, and some
	others show graphics corruption problems when displaying the initial installation splash
	screen.
	<br />
	If you encounter either of these issues, please file a bug report and give as much detail
	as possible, both about the symptoms and your hardware - this should assist the team to fix
	these bugs. As a workaround for now, try switching off UEFI and installing using <q>Legacy
	BIOS</q> or <q>Fallback mode</q> instead.
	<br />
	<b>Status:</b> More bug fixes might appear in the various Wheezy point releases.
	</dd>
-->

<!-- ditto...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>
