#use wml::debian::translation-check translation="8a5eb3970afae92528c267b43ff2cff70683b130" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>여러 취약점을 리눅스 커널에서 발견, 권한 상승, 서비스 거부 또는 정보 유출을 일으킬 수 있습니다.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3846">CVE-2019-3846</a>,
	<a href="https://security-tracker.debian.org/tracker/CVE-2019-10126">CVE-2019-10126</a>

    <p>huangwen reported multiple buffer overflows in the Marvell wifi
    (mwifiex) driver, which a local user could use to cause denial of
    service or the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5489">CVE-2019-5489</a>

    <p>Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari
    Trachtenberg, Jason Hennessey, Alex Ionescu, and Anders Fogh
    discovered that local users could use the mincore() system call to
    obtain sensitive information from other processes that access the
    same memory-mapped file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9500">CVE-2019-9500</a>,
	<a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

    <p>Hugues Anguelkov discovered a buffer overflow and missing access
    validation in the Broadcom FullMAC wifi driver (brcmfmac), which a
    attacker on the same wifi network could use to cause denial of
    service or the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11477">CVE-2019-11477</a>

    <p>Jonathan Looney reported that a specially crafted sequence of TCP
    selective acknowledgements (SACKs) allows a remotely triggerable
    kernel panic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>

    <p>Jonathan Looney reported that a specially crafted sequence of TCP
    selective acknowledgements (SACKs) will fragment the TCP
    retransmission queue, allowing an attacker to cause excessive
    resource usage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11479">CVE-2019-11479</a>

    <p>Jonathan Looney reported that an attacker could force the Linux
    kernel to segment its responses into multiple TCP segments, each of
    which contains only 8 bytes of data, drastically increasing the
    bandwidth required to deliver the same amount of data.</p>

    <p>This update introduces a new sysctl value to control the minimal MSS
    (net.ipv4.tcp_min_snd_mss), which by default uses the formerly hard    coded value of 48.  We recommend raising this to 536 unless you know
    that your network requires a lower value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

    <p>Jann Horn of Google reported numerous race conditions in the
    Siemens R3964 line discipline. A local user could use these to
    cause unspecified security impact. This module has therefore been
    disabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

    <p>Jann Horn of Google reported a race condition in the core dump
    implementation which could lead to a use-after-free.  A local
    user could use this to read sensitive information, to cause a
    denial of service (memory corruption), or for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11815">CVE-2019-11815</a>

    <p>It was discovered that a use-after-free in the Reliable Datagram
    Sockets protocol could result in denial of service and potentially
    privilege escalation.  This protocol module (rds) is not auto    loaded on Debian systems, so this issue only affects systems where
    it is explicitly loaded.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11833">CVE-2019-11833</a>

    <p>It was discovered that the ext4 filesystem implementation writes
    uninitialised data from kernel memory to new extent blocks.  A
    local user able to write to an ext4 filesystem and then read the
    filesystem image, for example using a removable drive, might be
    able to use this to obtain sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11884">CVE-2019-11884</a>

    <p>It was discovered that the Bluetooth HIDP implementation did not
    ensure that new connection names were null-terminated.  A local
    user with CAP_NET_ADMIN capability might be able to use this to
    obtain sensitive information from the kernel stack.</p></li>

</ul>

<p>안정 배포(stretch)에서, 이 문제를 버전 4.9.168-1+deb9u3에서 수정했습니다.</p>

<p>linux 패키지 업그레이드 권합니다.</p>

<p>linux의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4465.data"
