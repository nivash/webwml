#use wml::debian::translation-check translation="9bf43d9ebe1fe8f0f648cd5c0431b530d1105d92" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>여러 취약점을 아파치 HTTP 서버에서 발견했습니다.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17189">CVE-2018-17189</a>

    <p>Gal Goldshtein of F5 Networks discovered a denial of service
    vulnerability in mod_http2. By sending malformed requests, the
    http/2 stream for that request unnecessarily occupied a server
    thread cleaning up incoming data, resulting in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17199">CVE-2018-17199</a>

    <p>Diego Angulo from ImExHS discovered that mod_session_cookie does not
    respect expiry time.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0196">CVE-2019-0196</a>

    <p>Craig Young discovered that the http/2 request handling in mod_http2
    could be made to access freed memory in string comparison when
    determining the method of a request and thus process the request
    incorrectly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0211">CVE-2019-0211</a>

    <p>Charles Fol discovered a privilege escalation from the
    less-privileged child process to the parent process running as root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

    <p>A race condition in mod_auth_digest when running in a threaded
    server could allow a user with valid credentials to authenticate
    using another username, bypassing configured access control
    restrictions. The issue was discovered by Simon Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

    <p>Bernhard Lorenz of Alpha Strike Labs GmbH reported that URL
    normalizations were inconsistently handled. When the path component
    of a request URL contains multiple consecutive slashes ('/'),
    directives such as LocationMatch and RewriteRule must account for
    duplicates in regular expressions while other aspects of the servers
    processing will implicitly collapse them.</p></li>

</ul>

<p>안정배포(stretch)에서 이 문제를 버전 2.4.25-3+deb9u7에서 수정했습니다.
</p>

<p>이 업데이트는 다음 안정 포인트 릴리스에 넣기로 계획된 버그 픽스도 포함합니다.
이것은 버전 2.4.25-3+deb9u6에서 보안 수정에 의한 회귀용 수정을 포함합니다.</p>

<p>apache2 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>apache2의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오: <a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4422.data"
