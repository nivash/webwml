#use wml::debian::translation-check translation="cc3aa11466129a6224ab33a305a554cb8d65f63c"
<define-tag pagetitle>Atualização Debian 10: 10.6 lançado</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a sexta atualização de sua versão
estável (stable) do Debian <release> (codinome <q><codename></q>). 
Esta versão pontual adiciona principalmente correções para problemas de
segurança, além de pequenos ajustes para problemas sérios.
Avisos de segurança já foram publicados em separado e são referenciados 
quando necessário.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão
do Debian <release> mas apenas atualiza alguns pacotes já incluídos.
Não há necessidade de jogar fora as antigas mídias do <q><codename></q>
Após a instalação, os pacotes podem ser atualizados para as versões
mais atuais usando um espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir
do security.debian.org não precisarão atualizar muitos pacotes, 
e a maioria dessas atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais 
habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser
feita apontando o sistema de gerenciamento de pacotes para um dos muitos
espelhos HTTP do Debian. Uma lista abrangente de espelhos está disponível em:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções 
importantes nos seguintes pacotes.</p>

<p>Note que, devido a problemas de construção, as atualizações
para os pacotes cargo, rustc e rustc-bindgen não estão disponíveis 
para a arquitetura <q>armel</q>. Eles poderão ser adicionados posteriormente
se os problemas forem resolvidos.</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction arch-test "Correção de falha ocasional na detecção de s390x">
<correction asterisk "Correção de falha ao negociar T.38 com um fluxo 
recusado [CVE-2019-15297], <q>requisição SIP pode alterar endereço de um 
peer SIP</q> [CVE-2019-18790], <q>Usuário(a) AMI poderia executar comandos de
sistema</q> [CVE-2019-18610], segfault em pjsip mostra o histórico com
peers IPv6">
<correction bacula "Correção <q>uma string de resumo muito grande permite 
a um cliente malicioso causar um estouro de pilha na memória do director</q> 
[CVE-2020-11061]">
<correction base-files "Atualização de /etc/debian_version para a versão pontual">
<correction calamares-settings-debian "Desabilitado módulo displaymanager">
<correction cargo "Nova versão do aplicativo original (upstream), para
suporte a futuras versões Firefox ESR">
<correction chocolate-doom "Correção de validação ausente [CVE-2020-14983]">
<correction chrony "Prevenção de situação de condição de disputa quando 
escrevendo o PID em arquivo [CVE-2020-14367]; Correção da leitura de 
temperatura">
<correction debian-installer "Atualização Linux ABI para 4.19.0-11">
<correction debian-installer-netboot-images "Reconstruído a partir de 
proposed-updates">
<correction diaspora-installer "Usa opção --frozen para a o pacote de 
instalação utilizar a Gemfile.lock do aplicativo original; não excluir 
Gemfile.lock durante atualizações; não sobrescrever config/oidc_key.pem 
durante atualizações; tornar config/schedule.yml editável">
<correction dojo "Correção da poluição do protótipo no método deepCopy 
[CVE-2020-5258] e no método jqMix [CVE-2020-5259]">
<correction dovecot "Correção de regressão de sincronização do filtro sieve 
dsync; Correção no tratamento de resultado getpwent em userdb-passwd">
<correction facter "Mudança de Google GCE Metadata endpoint de <q>v1beta1</q>
para <q>v1</q>">
<correction gnome-maps "Correção de problema de renderização de camada de 
forma desalinhada">
<correction gnome-shell "LoginDialog: Reconfiguração de prompt de 
autenticação no switch VT antes de aparecimento gradual [CVE-2020-17489]">
<correction gnome-weather "Previne erro quando a configuração de localidades
é inválida">
<correction grunt "Usa safeLoad quando carregando arquivos YAML [CVE-2020-7729]">
<correction gssdp "Nova versão estável do aplicativo original (upstream)">
<correction gupnp "Nova  Versão estável do aplicativo original (upstream); 
Previne o ataque <q>CallStranger</q> [CVE-2020-12695]; requer GSSDP 1.0.5">
<correction haproxy "logrotate.conf: Usa rsyslog helper em vez do script 
SysV init; rejeita mensagens em que <q>chunked</q> está ausente de 
Transfer-Encoding [CVE-2019-18277]">
<correction icinga2 "Correção de ataque de link simbólico [CVE-2020-14004]">
<correction incron "Correção de limpeza de processos zumbis">
<correction inetutils "Correção de problema de execução de código remoto 
[CVE-2020-10188]">
<correction libcommons-compress-java "Correção de problema de negação 
de serviço [CVE-2019-12402]">
<correction libdbi-perl "Correção de corrupção de memória em função XS quando
pilha Perl é realocada [CVE-2020-14392]; correção de estouro de buffer em uma
classe DBD com nome excessivamente longo [CVE-2020-14393]; correção de 
desreferência de pefil nulo em dbi_profile() [CVE-2019-20919]">
<correction libvncserver "libvncclient: Abandona se o nome do socket UNIX 
causar sobrecarga [CVE-2019-20839]; Correção de problema de alinhamento/aliasing
[CVE-2020-14399]; limitação do tamanho máximo de textchat [CVE-2020-14405]; 
libvncserver: Adiciona checagem de ponteiros nulos ausentes [CVE-2020-14397]; 
correção de problem de aliasing/alinhamento [CVE-2020-14400]; scale: transforma
em 64 bit antes de mudança [CVE-2020-14401]; Previne acessos OOB 
[CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Correção de estouro de inteiro [CVE-2020-14344 
CVE-2020-14363]">
<correction lighttpd "Backport de diversas correções de usabilidade e 
segurança">
<correction linux "Nova versão estável do aplicativo original; Incremento de 
ABI para 11">
<correction linux-latest "Atualização para -11 para o kernel Linux ABI">
<correction linux-signed-amd64 "Nova versão estável do aplicativo original">
<correction linux-signed-arm64 "Nova versão estável do aplicativo original">
<correction linux-signed-i386 "Nova versão estável do aplicativo original">
<correction llvm-toolchain-7 "Nova versão estável do aplicativo original, 
para suporte à versão futura de Firefox ESR; correção de bugs afetando 
construção rustc">
<correction lucene-solr "Correção de problema de segurança na manipulação de
configuração em DataImportHandler [CVE-2019-0193]">
<correction milkytracker "Correção de heap overflow [CVE-2019-14464], estouro de pilha [CVE-2019-14496], heap overflow [CVE-2019-14497], uso após ficar livre [CVE-2020-15569]">
<correction node-bl "Correção de vulnerabilidade over-read [CVE-2020-8244]">
<correction node-elliptic "Prevenção de sobrecargas e maleabilidade [CVE-2020-13822]">
<correction node-mysql "Adição de opção localInfile para controle LOAD DATA 
LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse "Correção de validação insuficiente e sanitização 
de entrada de usuário(a) [CVE-2020-8124]">
<correction npm "Não mostrar senhas nos logs [CVE-2020-15095]">
<correction orocos-kdl "Remove inclusão explícita de caminho de inclusão padrão,
 corrigindo problemas com cmake &lt; 3.16">
<correction postgresql-11 "Nova versão estável do aplicativo original; 
Define um search_path seguro em replicação lógica walsenders e aplica workers
[CVE-2020-14349]; Scripts de instalação make contrib modules' mais seguros
[CVE-2020-14350]">
<correction postgresql-common "Não baixa plpgsql antes de testar extensões">
<correction pyzmq "Asyncio: aguarda por POLLOUT no remetente em can_connect">
<correction qt4-x11 "Correção de estouro de buffer no analisador XBM 
[CVE-2020-17507]">
<correction qtbase-opensource-src "Correção de estouro de buffer em 
analisador XBM [CVE-2020-17507]; Correção de quebra da área de transferência
quando o temporizador conclui depois de 50 dias">
<correction ros-actionlib "Carregamento seguro de YAML [CVE-2020-10289]">
<correction rustc "Nova versão estável do aplicativo original, para suporte 
à versão futura de Firefox ESR">
<correction rust-cbindgen "Nova versão estável do aplicativo original, para 
suporte à versão futura de Firefox ESR">
<correction ruby-ronn "Correção de manipulação de conteúdo UTF-8 nas manpages">
<correction s390-tools "Inserido no próprio código de dependência perl  
em vez de utilização de ${perl:Depends}, correção de instalação sob debootstrap">
</table>


<h2>Atualizações de segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão
estável (stable). A equipe de segurança já lançou um aviso para cada uma dessas 
atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>



<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as correções incorporadas na versão
estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informações da versão estável (stable) (notas de lançamento, errata etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional completamente livre Debian.</p>

<h2>Informações de Contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em 
<a href="$(HOME)/">https://www.debian.org/</a>, envie um email (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com o time de 
lançamento da versão estável (stable) em &lt;debian-release@lists.debian.org&gt;.</p>
