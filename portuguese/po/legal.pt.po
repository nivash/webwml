# Brazilian Portuguese translation for Debian website legal.pot
# Copyright (C) 2002-2008 Software in the Public Interest, Inc.
#
# Philipe Gaspar <philipegaspar@terra.com.br>, 2002
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2015-07-03 12:37-0300\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Informações de Licença"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "Índice DLS"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "DFSG FAQ"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Arquivo Debian-Legal"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s &ndash; %s, Versão %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Data de publicação"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Licença"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Versão"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Sumário"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Justificativa"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Discussão"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Sumário Original"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"O sumário original por <summary-author/> pode ser encontrado nos <a href="
"\"<summary-url/>\">arquivos da lista</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Este sumário foi preparado por <summary-author/>."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Texto da Licença (traduzido)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Texto da Licença"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "livre"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "não livre"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "não-redistribuível"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Livre"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Não-Livre "

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Não Redistribuível"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Veja a página de <a href=\"./\">informação sobre licenças</a> para uma visão "
"geral dos Sumários de Licença Debian (DLS)."
