msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: \n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Универсальная операционная система"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "Групповое фото с конференции DC19"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "Групповое фото с конференции DebConf19"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "MiniDebConf в Гамбурге, 2018 год"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "Групповое фото с конференции MiniDebConf в Гамбурге, 2018 год"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Снимок экрана программы установки Calamares"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Снимок экрана программы установки Calamares"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian похож на швейцарский нож"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "Люди хорошо проводят время с Debian"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""
"Участники Debian на конференции Debconf18 в Синьчжу действительно хорошо "
"проводят время"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Блог Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Блог"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Микроновости Debian"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Микроновости"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Планета Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Планета"
