<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a web cache poisoning attack in
Django, a popular Python-based web development framework.</p>

<p>This was caused by the unsafe handling of ";" characters in Python's
<tt>urllib.parse.parse_qsl</tt> method which had been backported to Django's
codebase to fix some other security issues in the past.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>

    <p>The package python/cpython from 0 and before 3.6.13, from 3.7.0 and
    before 3.7.10, from 3.8.0 and before 3.8.8, from 3.9.0 and before 3.9.2 are
    vulnerable to Web Cache Poisoning via urllib.parse.parse_qsl and
    urllib.parse.parse_qs by using a vector called parameter cloaking. When the
    attacker can separate query parameters using a semicolon (;), they can
    cause a difference in the interpretation of the request between the proxy
    (running with default configuration) and the server. This can result in
    malicious requests being cached as completely safe ones, as the proxy would
    usually not see the semicolon as a separator, and therefore would not
    include it in a cache key of an unkeyed parameter.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u11.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2569.data"
# $Id: $
