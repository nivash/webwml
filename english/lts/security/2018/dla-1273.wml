<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>simplesamlphp, an authentication and federation application has been
found vulnerable to Cross Site Scripting (XSS), signature validation
byepass and using insecure connection charset.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18121">CVE-2017-18121</a>

    <p>A Cross Site Scripting (XSS) issue has been found in the
    consentAdmin module of SimpleSAMLphp through 1.14.15, allowing an
    attacker to manually craft links that a victim can open, executing
    arbitrary javascript code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18122">CVE-2017-18122</a>

    <p>A signature-validation bypass issue was discovered in SimpleSAMLphp
    through 1.14.16. Service Provider using SAML 1.1 will regard as
    valid any unsigned SAML response containing more than one signed
    assertion, provided that the signature of at least one of the
    assertions is valid. Attributes contained in all the assertions
    received will be merged and the entityID of the first assertion
    received will be used, allowing an attacker to impersonate any user
    of any IdP given an assertion signed by the targeted IdP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6521">CVE-2018-6521</a>

    <p>The sqlauth module in SimpleSAMLphp before 1.15.2 relies on the
    MySQL utf8 charset, which truncates queries upon encountering
    four-byte characters. There might be a scenario in which this allows
    remote attackers to bypass intended access restrictions.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.2-1+deb7u2.</p>

<p>We recommend that you upgrade your simplesamlphp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1273.data"
# $Id: $
