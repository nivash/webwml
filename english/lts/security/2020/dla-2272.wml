<define-tag description>Debian 8 Long Term Support reaching end-of-life</define-tag>
<define-tag moreinfo>
<p>
The Debian Long Term Support (LTS) Team hereby announces that Debian 8
jessie support has reached its end-of-life on June 30, 2020,
five years after its initial release on April 26, 2015.
</p>
<p>
Debian will not provide further security updates for Debian 8. A
subset of jessie packages will be supported by external parties.
Detailed information can be found at <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.
</p>
<p>
The LTS Team will prepare the transition to Debian 9 stretch, which is the
current oldstable release. The LTS team has taken over support from the
Security Team on July 6, 2020 while the final point update for stretch will
be released on July 18, 2020.
</p>
<p>
Debian 9 will also receive Long Term Support for five years after its
initial release with support ending on June 30, 2022. The supported
architectures remain amd64, i386, armel and armhf. In addition we are
pleased to announce, for the first time support will be extended to
include the arm64 architecture.
</p>
<p>
For further information about using stretch LTS and upgrading from jessie
LTS, please refer to <a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
</p>
<p>
Debian and its LTS Team would like to thank all contributing users,
developers and sponsors who are making it possible to extend the life
of previous stable releases, and who have made this LTS a success.
</p>
<p>
If you rely on Debian LTS, please consider <a href="https://wiki.debian.org/LTS/Development">joining the team</a>,
providing patches, testing or <a href="https://wiki.debian.org/LTS/Funding">funding the efforts</a>.
</p>
<p>
More information about Debian Long Term Support can be found at
<a href="https://wiki.debian.org/LTS/">the LTS wiki page</a>.
</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2272.data"
# $Id: $
