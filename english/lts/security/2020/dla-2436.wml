<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in the sddm display manager
where local unprivileged users could create a connection to the X
server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28049">CVE-2020-28049</a>

    <p>An issue was discovered in SDDM before 0.19.0. It incorrectly starts the
    X server in a way that - for a short time period - allows local
    unprivileged users to create a connection to the X server without providing
    proper authentication. A local attacker can thus access X server display
    contents and, for example, intercept keystrokes or access the clipboard.
    This is caused by a race condition during Xauthority file
    creation.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.14.0-4+deb9u2.</p>

<p>We recommend that you upgrade your sddm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2436.data"
# $Id: $
