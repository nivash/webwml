<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In coturn before version 4.5.1.3, there is an issue whereby
STUN/TURN response buffer is not initialized properly. There
is a leak of information between different client connections.
One client (an attacker) could use their connection to
intelligently query coturn to get interesting bytes in the
padding bytes from the connection of another client.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.2.1.2-1+deb8u2.</p>

<p>We recommend that you upgrade your coturn packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2271.data"
# $Id: $
