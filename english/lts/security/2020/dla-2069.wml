<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were a number of cross-site scripting
vulnerabilities in cacti, a web interface for monitoring systems.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7106">CVE-2020-7106</a>

    <p>Cacti 1.2.8 has stored XSS in data_sources.php,
    color_templates_item.php, graphs.php, graph_items.php,
    lib/api_automation.php, user_admin.php, and user_group_admin.php, as
    demonstrated by the description parameter in data_sources.php (a raw string
    from the database that is displayed by $header to trigger the
    XSS).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.8.8b+dfsg-8+deb8u9.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2069.data"
# $Id: $
