<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>There were several CVE(s) reported against src:jackson-databind,
which are as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14060">CVE-2020-14060</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.5 mishandles the
    interaction between serialization gadgets and typing, related
    to oadd.org.apache.xalan.lib.sql.JNDIConnectionPool
    (aka apache/drill).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14061">CVE-2020-14061</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.5 mishandles the
    interaction between serialization gadgets and typing, related
    to oracle.jms.AQjmsQueueConnectionFactory,
    oracle.jms.AQjmsXATopicConnectionFactory,
    oracle.jms.AQjmsTopicConnectionFactory,
    oracle.jms.AQjmsXAQueueConnectionFactory, and
    oracle.jms.AQjmsXAConnectionFactory (aka weblogic/oracle-aqjms).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14062">CVE-2020-14062</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.5 mishandles the
    interaction between serialization gadgets and typing, related
    to com.sun.org.apache.xalan.internal.lib.sql.JNDIConnectionPool
    (aka xalan2).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14195">CVE-2020-14195</a>

    <p>FasterXML jackson-databind 2.x before 2.9.10.5 mishandles the
    interaction between serialization gadgets and typing, related
    to org.jsecurity.realm.jndi.JndiRealmFactory (aka org.jsecurity).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.2-2+deb8u15.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2270.data"
# $Id: $
