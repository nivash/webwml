<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It has been found that rubygem archive-tar-minitar allows attackers to
overwrite arbitrary files during archive extraction via a .. (dot dot)
in an extracted filename.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.5.2-2+deb7u1.</p>

<p>We recommend that you upgrade your ruby-archive-tar-minitar packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-808.data"
# $Id: $
