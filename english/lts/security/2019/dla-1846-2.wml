<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The unzip security update issued as DLA 1846-1 caused a regression
when building the Firefox web browser from source.</p>

<p>There is a zip-like file in the Firefox distribution, omni.ja, which is
a zip container with the central directory placed at the start of the
file instead of after the local entries as required by the zip standard.
This update now permits such containers to not raise a zip bomb alert,
where in fact there are no overlaps.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
6.0-16+deb8u5.</p>

<p>We recommend that you upgrade your unzip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1846-2.data"
# $Id: $
