<define-tag pagetitle>General Resolution: Statement regarding Richard Stallman's readmission to the FSF board</define-tag>
<define-tag status>V</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Proposal and amendment</th>
        <td>2021-03-24</td>
		<td></td>
      </tr>
      <tr>
        <th>Discussion Period:</th>
		<td>2021-03-24</td>
		<td></td>
      </tr>
          <tr>
            <th>Voting period:</th>
            <td>Sunday 2021-04-04 00:00:00 UTC</td>
            <td>Saturday 2021-04-17 23:59:59 UTC</td>
    </table>

    The Project Leader has varied minimum discussion period to [<a href='https://lists.debian.org/debian-vote/2021/03/msg00115.html'>1 week</a>]

    <vproposer />
    <p>Steve Langasek [<email vorlon@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00083.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00095.html'>amendment</a>]
    </p>
    <vseconds />
    <ol>
	<li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00084.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00097.html'>confirm</a>] </li>
	<li>Joerg Jaspert [<email joerg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00085.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00100.html'>confirm</a>] </li>
	<li>Neil McGovern [<email neilm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00086.html'>mail</a>] </li>
	<li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00087.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00096.html'>confirm</a>] </li>
	<li>Sam Hartman [<email hartmans@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00088.html'>mail</a>] </li>
	<li>Nicolas Dandrimont [<email olasd@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00091.html'>mail</a>] </li>
	<li>Colin Tuckley [<email colint@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00092.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00098.html'>confirm</a>]</li>
	<li>Paul R. Tagliamonte [<email paultag@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00161.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00103.html'>confirm</a>]</li>
    </ol>
    <vtext />
	<h3>Choice 1: Call for the FSF board removal, as in rms-open-letter.github.io</h3>

<p>The Debian Project co-signs the statement regarding Richard Stallman's
readmission to the FSF board seen at
<a href='https://github.com/rms-open-letter/rms-open-letter.github.io/blob/main/index.md'>https://github.com/rms-open-letter/rms-open-letter.github.io/blob/main/index.md</a>.
The text of this statement is given below.</p>

<p>Richard M. Stallman, frequently known as RMS, has been a dangerous force in
the free software community for a long time.  He has shown himself to be
misogynist, ableist, and transphobic, among other serious accusations of
impropriety.  These sorts of beliefs have no place in the free software,
digital rights, and tech communities.  With his recent reinstatement to the
Board of Directors of the Free Software Foundation, we call for the entire
Board of the FSF to step down and for RMS to be removed from all leadership
positions.</p>

<p>We, the undersigned, believe in the necessity of digital autonomy and the
powerful role user freedom plays in protecting our fundamental human rights.
In order to realize the promise of everything software freedom makes
possible, there must be radical change within the community.  We believe in
a present and a future where all technology empowers - not oppresses -
people.  We know that this is only possible in a world where technology is
built to pay respect to our rights at its most foundational levels.  While
these ideas have been popularized in some form by Richard M. Stallman, he
does not speak for us.  We do not condone his actions and opinions.  We do
not acknowledge his leadership or the leadership of the Free Software
Foundation as it stands today.

<p>There has been enough tolerance of RMS’s repugnant ideas and behavior.  We
cannot continue to let one person ruin the meaning of our work.  Our
communities have no space for people like Richard M. Stallman, and we will
not continue suffering his behavior, giving him a leadership role, or
otherwise holding him and his hurtful and dangerous ideology as acceptable.

<p>We are calling for the removal of the entire Board of the Free Software
Foundation.  These are people who have enabled and empowered RMS for years.
They demonstrate this again by permitting him to rejoin the FSF Board.  It
is time for RMS to step back from the free software, tech ethics, digital
rights, and tech communities, for he cannot provide the leadership we need.
We are also calling for Richard M. Stallman to be removed from all
leadership positions, including the GNU Project.

<p>We urge those in a position to do so to stop supporting the Free Software
Foundation.  Refuse to contribute to projects related to the FSF and RMS.
Do not speak at or attend FSF events, or events that welcome RMS and his
brand of intolerance.  We ask for contributors to free software projects to
take a stand against bigotry and hate within their projects.  While doing
these things, tell these communities and the FSF why.

<p><a href='https://rms-open-letter.github.io/appendix'>We have detailed several public incidents of RMS's behavior.</a>  Some of us
have our own stories about RMS and our interactions with him, things that
are not captured in email threads or on video.  We hope you will read what
has been shared and consider the harm that he has done to our community and
others.</p>


    <vproposerb />
    <p>Sruthi Chandran [<email srud@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00246.html'>text of proposal</a>]
    </p>
    <vsecondsb />
    <ol>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00253.html'>mail</a>] </li>
	<li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00261.html'>mail</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00265.html'>mail</a>] </li>
	<li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00266.html'>mail</a>] </li>
	<li>Mike Gabriel [<email sunweaver@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00267.html'>mail</a>] </li>
	<li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00269.html'>mail</a>] </li>
	<li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00270.html'>mail</a>] </li>
	<li>Gard Spreemann [<email gspr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00272.html'>mail</a>] </li>
	<li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00283.html'>mail</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00292.html'>mail</a>] </li>
    </ol>
    <vtextb />
	<h3>Choice 2: Call for Stallman's resignation from all FSF bodies</h3>

<p>Under section 4.1.5 of the constitution, the Developers make the
following statement:</p>

<b>Debian’s statement on Richard Stallman rejoining the FSF board</b>

<p>We at Debian are profoundly disappointed to hear of the re-election of
Richard Stallman to a leadership position at the Free Software
Foundation, after a series of serious accusations of misconduct led to
his resignation as president and board member of the FSF in 2019.</p>

<p>One crucial factor in making our community more inclusive is to
recognise and reflect when other people are harmed by our
own actions and consider this feedback in future actions. The way
Richard Stallman announced his return to the board unfortunately lacks
any acknowledgement of this kind of thought process. We are deeply
disappointed that the FSF board elected him a board member again despite
no discernible steps were taken
by him to be accountable for, much less make amends for, his past
actions or those who have been harmed by them. Finally, we are also
disturbed by the secretive process of his re-election, and how it was
belatedly conveyed [0] to FSF’s staff and supporters.</p>

<p>We believe this step and how it was communicated sends wrong and hurtful
message and harms the future of the Free Software movement. The goal of
the software freedom movement is to empower all people to control
technology and thereby create a better society for everyone. Free
Software is meant to serve everyone regardless of their age, ability or
disability, gender identity, sex, ethnicity, nationality, religion or
sexual orientation. This requires an inclusive and diverse environment
that welcomes all contributors equally. Debian realises that we
ourselves and the Free Software movement still have to work hard to be
in that place where everyone feels safe and respected to participate in
it in order to fulfil the movement's mission.<p>

<p>That is why, we call for his resignation from all FSF bodies. The FSF
needs to seriously reflect on this decision as well as their
decision-making process to prevent similar issues from happening again.
Therefore, in the current situation we see ourselves unable to
collaborate both with the FSF and any other organisation in which
Richard Stallman has a leading position. Instead, we will continue to
work with groups and individuals who foster diversity and equality in
the Free Software movement in order to achieve our joint goal of
empowering all users to control technology.</p>

[0] <a href='https://status.fsf.org/notice/3796703'>https://status.fsf.org/notice/3796703</a>

<br>
Heavily based on:<br>

[1] <a href='https://fsfe.org/news/2021/news-20210324-01.html'>https://fsfe.org/news/2021/news-20210324-01.html</a>
<br>
[2] <a href='https://www.eff.org/deeplinks/2021/03/statement-re-election-richard-stallman-fsf-board'>https://www.eff.org/deeplinks/2021/03/statement-re-election-richard-stallman-fsf-board</a>

    <vproposerc />
    <p>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00344.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00376.html'>amendment</a>]
    </p>
    <vsecondsc />
    <ol>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00345.html'>mail</a>] </li>
	<li>Milan Kupcevic [<email milan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00347.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00379.html'>confirm</a>]</li>
	<li>Apollon Oikonomopoulos [<email apoikos@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00351.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00385.html'>confirm</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00353.html'>mail</a>] </li>
	<li>Zlatan Todoric [<email zlatan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00363.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00377.html'>confirm</a>]</li>
    </ol>
    <vtextc />
	<h3>Choice 3: Discourage collaboration with the FSF while Stallman is in a leading position</h3>

<p>Under section 4.1.5 of the constitution, the Developers make the following
statement:</p>

<p>Debian’s statement on Richard Stallman rejoining the FSF board</p>

<p>We at Debian are profoundly disappointed to hear of the re-election of Richard
Stallman to a leadership position at the Free Software Foundation, after a
series of serious accusations of misconduct led to his resignation as
president and board member of the FSF in 2019.</p>

<p>One crucial factor in making our community more inclusive is to recognise and
reflect when other people are harmed by our actions and consider this feedback
in future actions. Unfortunately, the way Richard Stallman announced his
return to the board lacks any acknowledgement of this kind of thought process.
We are deeply disappointed that the FSF board elected him as a board member
again, despite no discernible steps were taken by him to be accountable for,
much less make amends for, his past actions or those who have been harmed by
them. Finally, we are also disturbed by the secretive process of his
re-election, and how it was belatedly conveyed [0] to FSF’s staff and
supporters.</p>

<p>We believe this step and how it was communicated sends a wrong and hurtful
message and harms the future of the Free Software movement. The goal of the
software freedom movement is to empower all people to control technology and
thereby create a better society for everyone. Free Software is meant to serve
everyone regardless of their age, ability or disability, gender identity, sex,
ethnicity, nationality, religion or sexual orientation. This requires an
inclusive and diverse environment that welcomes all contributors equally.
Debian realises that we and the Free Software movement still have to work hard
to be in that place where everyone feels safe and respected to participate in
it in order to fulfil the movement's mission.</p>

<p>Therefore, in the current situation, the Debian Project discourages
collaborating both with the FSF and any other organisation in which
Richard Stallman has a leading position. Instead, we will continue
looking forward to work with groups and individuals who foster diversity
and equality in the Free Software movement in order to achieve our joint
goal of empowering all users to control technology.</p>

<p>[0] <a href='https://status.fsf.org/notice/3796703'>https://status.fsf.org/notice/3796703</a></p>

    <vproposerd />
    <p>Jonathan Wiltshire [<email jmw@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00391.html'>text of proposal</a>]
    </p>
    <vsecondsd />
    <ol>
	<li>Nicolas Dandrimont [<email olasd@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00393.html'>mail</a>] </li>
	<li>Thadeu Lima de Souza Cascardo [<email cascardo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00395.html'>mail</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00396.html'>mail</a>] </li>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00401.html'>mail</a>] </li>
	<li>Zlatan Todoric [<email zlatan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00402.html'>mail</a>] </li>
	<li>Paride Legovini [<email paride@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00404.html'>mail</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00405.html'>mail</a>] </li>
	<li>Micha Lenk [<email micha@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00406.html'>mail</a>] </li>
	<li>Gard Spreemann [<email gspr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00408.html'>mail</a>] </li>
	<li>Milan Kupcevic [<email milan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00410.html'>mail</a>] </li>
	<li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00418.html'>mail</a>] </li>
	<li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00422.html'>mail</a>] </li>
	<li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00066.html'>mail</a>] </li>
    </ol>
    <vtextd />
	<h3>Choice 4: Call on the FSF to further its governance processes</h3>

<p>This is a position statement of the Debian Developers in accordance with
our constitution, section 4.1.5.</p>

<p>The Developers firmly believe that leaders in any prominent organisation
are, and should be, held to the highest standards of accountability.</p>

<p>We are disappointed that issues of transparency and accountability in the
governance of the Free Software Foundation have led to unresolved and
serious complaints of impropriety by its founder Richard Stallman over a
number of years whilst in the position of president and as a member of the
board. In particular, we are deeply concerned that the board saw fit to
reinstate him without properly considering the effect of its actions on
those complainants.</p>

<p>The Developers acknowledge that people make mistakes but believe that where
those people are in leadership positions, they must be held accountable for
their mistakes. We believe that the most important part of making mistakes
is learning from them and changing behaviour. We are most concerned that
Richard and the board have not sufficiently acknowledged or learned from
issues which have affected a large number of people and that Richard
remains a significant influence on both the FSF board and the GNU project.</p>

<p>We call upon the Free Software Foundation to further steps it has taken in
March 2021 to overhaul governance of the organisation, and to work
tirelessly to ensure its aim is fulfilled. We believe that only through
properly accountable governance can members of an organisation ensure their
voice is heard. The Free Software Foundation must do everything in its
power to protect its staff and members, and the wider community, including
a robust and transparent process for dealing with complaints.</p>

<p>We urge Richard Stallman and the remaining members of the board which
reinstated him, to consider their positions.</p>

<p>The Developers are proud that contributors to free software come from all
walks of life and that our diverse experience and opinions are a strength
of software freedom. But we must never cease in our efforts to ensure that
all contributors are treated with respect, and that they feel safe and
secure in our communities - including when we meet in person.</p>

    <vproposere />
    <p>Timo Weingärtner [<email tiwe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00195.html'>text of proposal</a>]
    </p>
    <vsecondse />
    <ol>
	<li>Axel Beckert [<email abe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00239.html'>mail</a>] </li>
	<li>Lionel Élie Mamane [<email lmamane@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00338.html'>mail</a>] </li>
	<li>Adam Borowski [<email kilobyte@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00359.html'>mail</a>] </li>
	<li>Dmitry Smirnov [<email onlyjob@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00008.html'>mail</a>] </li>
	<li>Erik Schanze [<email eriks@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00009.html'>mail</a>] </li>
    </ol>
    <vtexte />
	<h3>Choice 5: Support Stallman's reinstatement, as in rms-support-letter.github.io</h3>
<p>The Debian Project co-signs the statement regarding Richard Stallman's
readmission to the FSF board seen at <a href='https://rms-support-letter.github.io/'>https://rms-support-letter.github.io/</a>.
The text of this statement is given below.</p>

<p>Richard M. Stallman, frequently known as RMS, has been a driving force in the
free software movement for decades, with contributions including the GNU
operating system and Emacs.</p>

<p>Recently, there have been vile online attacks looking to remove him from the
FSF board of directors for expressing his personal opinions. We have watched
this happen before in an organized fashion with other prominent free software
activists and programmers. We will not stand idly this time, when an icon of
this community is attacked.</p>

<p>FSF is an autonomous body that is capable of treating its members in a fair,
unbiased fashion, and should not give in to external social pressures. We urge
the FSF to consider the arguments against RMS objectively and to truly
understand the meaning of his words and actions.</p>

<p>Historically, RMS has been expressing his views in ways that upset many
people. He is usually more focused on the philosophical underpinnings, and
pursuing the objective truth and linguistic purism, while underemphasising
people’s feelings on matters he’s commenting on. This makes his arguments
vulnerable to misunderstanding and misrepresentation, something which we feel
is happening in the open letter calling for his removal. His words need to be
interpreted in this context and taking into account that more often than not,
he is not looking to put things diplomatically.</p>

<p>Regardless, Stallman’s opinions on the matters he is being persecuted over are
not relevant to his ability to lead a community such as the FSF. Furthermore,
he is entitled to his opinions just as much as anyone else. Members and
supporters do not have to agree with his opinions, but should respect his
right to freedom of thought and speech.</p>

<h4>To the FSF:</h4>

<p>Removing RMS will hurt FSF’s image and will deal a significant blow to the
momentum of the free software movement. We urge you to consider your actions
carefully, as what you will decide will have a serious impact on the future of
the software industry.</p>

<h4>To the ambush mob who is ganging up on Richard Stallman over reasonable
arguments in debate and various opinions and beliefs voiced over decades as a
public figure:</h4>

<p>You have no part in choosing the leadership of any communities. Especially not
via another mob attack which does not remotely resemble a fairly conducted
debate as exemplified by better people such as Richard Stallman.</p>

    <vproposerf />
    <p>Craig Sanders [<email cas@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00097.html'>text of proposal</a>]
    </p>
    <vsecondsf />
    <ol>
	<li>Adrian Bunk [<email bunk@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00187.html'>mail</a>]</li>
	<li>Norbert Preining [<email preining@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00188.html'>mail</a>]</li>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00189.html'>mail</a>]</li>
	<li>Ying-Chun Liu [<email paulliu@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00190.html'>mail</a>]</li>
	<li>Barak A. Pearlmutter [<email bap@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00191.html'>mail</a>]</li>
	<li>Adam Borowski [<email kilobyte@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00196.html'>mail</a>]</li>
	<li>Micha Lenk [<email micha@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00200.html'>mail</a>]</li>
	<li>Michael Biebl [<email biebl@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00209.html'>mail</a>]</li>
    </ol>
    <vtextf />
	<h3>Choice 6: Denounce the witch-hunt against RMS and the FSF</h3>

<p>Debian refuses to participate in and denounces the witch-hunt against Richard
Stallman, the Free Software Foundation, and the members of the board of the
Free Software Foundation.</p>

    <vproposera />
    <p>Timo Weingärtner [<email tiwe@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00196.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00200.html'>amendment 1</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00224.html'>amendment 2</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00297.html'>amendment 3</a>]
    </p>
    <vsecondsa />
    <ol>
	<li>Bart Martens [<email bartm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00201.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00322.html'>confirm</a>]</li>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00207.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00212.html'>confirm</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00232.html'>confirm</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00300.html'>confirm</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00215.html'>mail</a>] </li>
        <li>Daniel Lenharo [<email lenharo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00219.html'>mail</a>] </li>
	<li>Milan Kupcevic [<email milan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00228.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00304.html'>confirm</a>]</li>
	<li>Michael Biebl [<email biebl@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00235.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00301.html'>confirm</a>]</li>
	<li>Axel Beckert [<email abe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00236.html'>mail</a>] </li>
	<li>Gilles Filippini [<email pini@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00241.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00327.html'>confirm</a>] </li>
	<li>Filippo Rusconi [<email lopippo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00252.html'>mail</a>] </li>
	<li>Shengjing Zhu [<email zhsj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00260.html'>mail</a>] </li>
	<li>Matteo F. Vescovi [<email mfv@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00268.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00314.html'>confirm</a>] </li>
	<li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00299.html'>mail</a>] </li>
    </ol>
    <vtexta />
	<h3>Choice 7: Debian will not issue a public statement on this issue</h3>

<p>The Debian Project will not issue a public statement on whether Richard
Stallman should be removed from leadership positions or not.</p>

<p>Any individual (including Debian members) wishing to (co-)sign any of the open
letters on this subject is invited to do this in a personal capacity.</p>

#    <vquorum />
#     <p>
#        With the current list of <a href="vote_002_quorum.log">voting
#          developers</a>, we have:
#     </p>
#    <pre>
##include 'vote_002_quorum.txt'
#    </pre>
##include 'vote_002_quorum.src'
#
#
#
    <vstatistics />
    <p>
	For this GR, like always,
                <a href="https://vote.debian.org/~secretary/gr_rms/">statistics</a>
#               <a href="suppl_002_stats">statistics</a>
             will be gathered about ballots received and
             acknowledgements sent periodically during the voting
             period.
#               Additionally, the list of <a
#             href="vote_002_voters.txt">voters</a> will be
#             recorded. Also, the <a href="vote_002_tally.txt">tally
#             sheet</a> will also be made available to be viewed.
         </p>

    <vmajorityreq />
    <p>
      The proposals need a simple majority
    </p>
##include 'vote_002_majority.src'
#
#    <voutcome />
##include 'vote_002_results.src'
#
    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>

