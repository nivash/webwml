#use wml::debian::template title="Informationen zur Benutzung der Suchmaschine"
#use wml::debian::translation-check translation="c3b565ac6b9deb00572865c557a9c7094047163a"
# Translator: Martin Schulze <joey@debian.org>
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019.
# $Id$

<p>Die Debian-Suchmaschine auf <a
href="https://search.debian.org/">https://search.debian.org/</a> erlaubt
verschiedene Arten der Suche, abhängig davon, was Sie suchen.</p>

<h3>Einfache Suche</h3>

<p>Die einfachste Art von allen besteht darin, ein einziges Wort in
das Suchfeld einzugeben und Enter zu drücken (bzw. auf den Button
<em>Search</em> zu klicken). Die Suchmaschine wird dann alle Seiten
der Website zurückliefern, die das Wort enthalten. Dies wird Ihnen
meistens gute Resultate liefern.</p>

<p>Die nächste Stufe betrifft die Suche nach mehr als einem Wort,
sie wird die Seiten zurückliefern, die alle eingegebenen Wörter enthalten.</p>

<h3>Boole'sche Suche</h3>

<p>Wenn die einfache Suche nicht ausreichend ist, dann kann
<a href="https://foldoc.org/boolean">Boolean</a>
Ihnen möglicherweise weiterhelfen. Sie haben die Wahl zwischen
<em>AND</em>, <em>OR</em>, <em>NOT</em> und einer Kombination dieser drei.
Beachten Sie, dass die Operatoren alle ausschließlich in Großbuchstaben
geschrieben werden müssen, um erkannt zu werden.</p>

<p><B>AND</B> gibt die Seiten zurück, die beide
Suchbegriffe enthalten. Beispiel: <q>gcc AND patch</q> gibt alle
Seiten aus, die die Wörter <q>gcc</q> und <q>patch</q> enthalten.
Dieses Beispiel gibt die gleichen Ergebnisse aus wie <q>gcc patch</q>, aber
die explizite Angabe des AND kann in Kombination mit anderen Operatoren
nützlich sein.</p>

<p><B>OR</B> gibt die Seiten zurück, die einen der
beiden Suchbegriffe enthalten. Beispiel: <q>gcc OR patch</q> gibt alle
Seiten aus, in denen <q>gcc</q> oder <q>patch</q> enthalten ist.</p>

<p><B>NOT</B> schließt ein Wort von den Ergebnissen aus.
Beispiel: <q>gcc NOT patch</q> wird alle Seiten finden, die den Suchbegriff
<q>gcc</q> enthalten, jedoch nicht gleichzeitig den Suchbegriff <q>patch</q>.
Sie können auch schreiben <q>gcc AND NOT patch</q>, was das gleiche Ergebnis
ausgibt, aber eine Suche lediglich nach <q>NOT patch</q> wird nicht
unterstützt.</p>

<p><B>(</B>...<B>)</B> kann verwendet werden, um Bedingungen zu gruppieren.
Beispiel: <q>(gcc OR make) NOT patch</q> wird alle Seiten finden, die entweder
<q>gcc</q> oder <q>make</q> enthalten, jedoch nicht <q>patch</q>.</p>
