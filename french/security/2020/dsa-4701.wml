#use wml::debian::translation-check translation="90833ca5169a5ef4cdeac320dd3d7016a5d5f8d2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour fournit le microcode de processeur mis à jour pour
certains types de processeurs Intel et des mitigations pour les
vulnérabilités matérielles « Special Register Buffer Data Sampling »
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>),
« Vector Register Sampling »
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>)
et « L1D Eviction Sampling »
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>).</p>

<p>La mise à jour du microcode pour les processeurs HEDT et Xeon avec la
signature 0x50654 qui a été annulée dans DSA 4565-2 est maintenant incluse
avec une version corrigée.</p>

<p>La mise à jour amont pour Skylake-U/Y (signature 0x406e3) a été exclue
de cette mise à jour du fait du signalement de blocages au démarrage.</p>

<p>Pour plus de détails, reportez vous
à <a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html</a> et
à <a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html</a>.</p>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 3.20200609.2~deb9u1.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 3.20200609.2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4701.data"
# $Id: $
