#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvés dans rubygems, paquets a cadriciel management
for Ruby. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000075">CVE-2018-1000075</a>

<p>Une vulnérabilité de taille négative dans l’en-tête Ruby d’archive de paquet
gem pourrait causer une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000076">CVE-2018-1000076</a>

<p>Le paquet gems de Raby ne vérifie pas correctement les signatures chiffrées.
Un gem de signature incorrecte pourrait être installé si l’archive contient
plusieurs signatures gem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000077">CVE-2018-1000077</a>

<p>Une vulnérabilité de validation incorrecte dans l’attribut de la page
d’accueil de la spécification gems de Ruby pourrait permettre à un gem
malveillant de définir une URL de page d’accueil non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000078">CVE-2018-1000078</a>

<p>Une vulnérabilité de script intersite (XSS) dans l’affichage du serveur gem
de l’attribut de page d’accueil.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.8.24-1+deb7u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets rubygems.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1336.data"
# $Id: $
