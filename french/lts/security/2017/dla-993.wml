#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0605">CVE-2017-0605</a>

<p>Un dépassement de tampon défaut a été découvert dans le sous-système trace.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7487">CVE-2017-7487</a>

<p>Li Qiang a signalé une fuite de compteur de références dans la fonction
ipxitf_ioctl qui peut avoir pour conséquence une vulnérabilité d'utilisation
de mémoire après libération, déclenchable lors de la configuration d'une
interface IPX.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7645">CVE-2017-7645</a>

<p>Tuomas Haanpaa et Matti Kamunen de Synopsys Ltd ont découvert que
les implémentations de serveurs NFSv2 et NFSv3 étaient vulnérables à un
problème d'accès mémoire hors limites lors du traitement d'arguments
arbitrairement longs envoyés par des clients PRC NFSv2 ou NFSv3, menant à un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7895">CVE-2017-7895</a>

<p>Ari Kauppi de Synopsys Ltd a découvert que les implémentations de
serveurs NFSv2 et NFSv3 ne géraient pas correctement la vérification des
limites de charge utile des requêtes WRITE. Un attaquant distant avec des
droits d'accès en écriture sur un montage NFS peut tirer avantage de ce
défaut pour lire des morceaux de mémoire arbitraires à partir à la fois de
l'espace noyau et de l'espace utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8890">CVE-2017-8890</a>

<p>La fonction net_csk_clone_lock() permet à un attaquant distant de
provoquer une double libération de zone de mémoire menant à un déni de
service ou, éventuellement, avoir un autre impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8924">CVE-2017-8924</a>

<p>Johan Hovold a découvert que le pilote série USB io_ti pourrait divulguer
des informations sensibles lors de la connexion d'un périphérique USB
malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8925">CVE-2017-8925</a>

<p>Johan Hovold a découvert une fuite de compteur de références dans le
pilote série USB omninet, avec pour conséquence une vulnérabilité
d'utilisation de mémoire après libération. Cela peut être déclenché par un
utilisateur local autorisé à ouvrir des périphériques tty.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9074">CVE-2017-9074</a>

<p>Andrey Konovalov a signalé que l'implémentation de la fragmentation
d'IPv6 pourrait lire au-delà de la fin d'un tampon de paquet. Un utilisateur
local ou une VM cliente pourraient être capables d'utiliser cela pour la
divulgation d'informations sensibles ou pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9075">CVE-2017-9075</a>

<p>Andrey Konovalov a signalé que l'implémentation de SCTP/IPv6 initialisait
mal les listes d'adresses sur les sockets connectées, avec pour conséquence
une vulnérabilité d'utilisation de mémoire après libération, un problème
similaire
à <a href="https://security-tracker.debian.org/tracker/CVE-2017-8890">CVE-2017-8890</a>.
Cela peut être déclenché par n'importe quel utilisateur local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9076">CVE-2017-9076</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9077">CVE-2017-9077</a>

<p>Cong Wang a découvert que les implémentations de TCP/IPv6 et de DCCP/IPv6
initialisaient mal les listes d'adresses sur les sockets connectées, un
problème similaire
à <a href="https://security-tracker.debian.org/tracker/CVE-2017-9075">CVE-2017-9075</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9242">CVE-2017-9242</a>

<p>Andrey Konovalov a signalé un débordement de tampon de paquet dans
l'implémentation d'IPv6. Un utilisateur local pourrait utiliser cela pour un
déni de service (corruption de mémoire, plantage) et éventuellement pour une
augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000364">CVE-2017-1000364</a>

<p>Qualys Research Labs a découvert que la taille de la page de protection
de pile (« stack guard page ») n'est pas suffisamment grande. Le pointeur de
pile peut franchir la page de protection et passer de la pile à un autre
espace mémoire sans accéder à la page de protection. Dans ce cas aucune
exception de faute de page n'est levée et la pile s'étend dans l'autre
espace de la mémoire. Un attaquant peut exploiter ce défaut pour une
augmentation de droits.</p>

<p>La protection d'espace de pile par défaut est réglée à 256 pages et peut
être configurée sur la ligne commande du noyau grâce au paramètre
stack_guard_gap du noyau.</p>

<p>Vous trouverez plus de détails à l'adresse
<a href="https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt">https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt</a></p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.2.89-1.Cette version inclut aussi des corrections de bogue de la
version 3.2.89 de l’amont.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.43-2+deb8u1.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.9.30-2+deb9u1 ou les précédentes versions avant la publication de
<q>Stretch</q>.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-993.data"
# $Id: $
