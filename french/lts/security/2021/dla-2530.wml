#use wml::debian::translation-check translation="7c467aa82cca1fc5ff5eac3d73a6220d40ef0906" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Drupal a repéré une vulnérabilité dans la version de la bibliothèque
Archive_Tar fournie
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-36193">CVE-2020-36193</a>),
qui crée des vulnérabilités d’extraction hors chemin, comme indiqué dans
l’annonce de sécurité de Drupal d’identifiant SA-CORE-2021-001 :</p>
<p>https://www.drupal.org/sa-core-2021-001</p>

<p> Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été résolus dans la
version 7.52-2+deb9u14 de drupal7.</p>

<p>Pour Debian 9 <q>Stretch</q>, le correctif a été rétroporté dans
la version 7.52-2+deb9u14.</p>

<p>Nous vous recommandons de mettre à niveau vos paquets drupal7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de drupal7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
https://security-tracker.debian.org/tracker/source-package/drupal7</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>


</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2530.data"
# $Id: $
