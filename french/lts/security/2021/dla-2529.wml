#use wml::debian::translation-check translation="cb417b316b3c5f87f2ebe8872f73373c1bee4074" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La RFC 822.c dans Mutt jusqu’à la version 2.0.4 permet à des attaquants
distants de provoquer un déni de service (indisponibilité de boîte aux lettres)
en envoyant des messages de courriel avec des séries de points-virgules dans les
champs d’adresse RFC 822 (c'est-à-dire la terminaison de groupes vides).</p>

<p>Un message de courriel court d’un attaquant peut provoquer une consommation
abondante de mémoire et la victime peut alors être incapable de voir les
messages de courriel d’autres personnes.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.7.2-1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mutt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mutt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mutt">https://security-tracker.debian.org/tracker/mutt</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2529.data"
# $Id: $
