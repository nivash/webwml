#use wml::debian::translation-check translation="c5e34f22532513afbcd94966130dadbb32cfa806" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans jruby, une implémentation
en Java du langage de programmation Ruby.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000074">CVE-2018-1000074</a>

<p>Vulnérabilité de désérialisation de données non sûre dans la commande
 owner qui peut aboutir à l’exécution de code. Cette attaque apparait être
exploitable à l’aide de la victime devant exécuter la commande « gem owner »
dans un gem avec un fichier YAML contrefait pour l’occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000075">CVE-2018-1000075</a>

<p>Boucle infinie causée par une vulnérabilité de taille négative dans les
en-têtes tar de paquets gem de Ruby pouvant aboutir à une taille négative
et causer une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000076">CVE-2018-1000076</a>

<p>Vulnérabilité de vérification incorrecte de signature chiffrée dans
package.rb pouvant aboutir à un gem mal signé installé, car l’archive compressée
pourrait contenir plusieurs signatures de gem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000077">CVE-2018-1000077</a>

<p>Vulnérabilité de validation incorrecte d’entrée dans l’attribut de la page
d’accueil de la spécification RubyGems pouvant permettre à un gem malveillant
de définir une URL de page d’accueil non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000078">CVE-2018-1000078</a>

<p>Vulnérabilité de script intersite (XSS) dans l’affichage du serveur de gem
de l’attribut de page d’accueil. Cette attaque apparait être exploitable
à l’aide de la victime devant parcourir un gem malveillant sur un serveur gem
vulnérable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8321">CVE-2019-8321</a>

<p>Appels Gem::UserInteraction#verbose exprimés sans protection, une
injection de séquence d’échappement était possible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8322">CVE-2019-8322</a>

<p>La commande owner de gem affiche le contenu de la réponse de l’API
directement sur la sortie standard. Par conséquent, si la réponse est
contrefaite, une injection de séquence d’échappement peut se produire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8323">CVE-2019-8323</a>

<p>Gem::GemcutterUtilities#with_response peut afficher la réponse de l’API sur
la sortie standard telle quelle. Par conséquent, si le côté API modifie la
réponse, une injection de séquence d’échappement peut se produire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8324">CVE-2019-8324</a>

<p>Un gem contrefait avec un nom multi-ligne n’est pas géré correctement. Par
conséquent, un attaquant pourrait injecter du code arbitraire sur la ligne de
souche de gemspec, qui est évalué par du code dans ensure_loadable_spec lors
la vérification de préinstallation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8325">CVE-2019-8325</a>

<p>Gem::CommandManager#run appelle alert_error sans échappement, une injection de
séquence d’échappement est possible. (Il existe plusieurs façons de provoquer
cette erreur).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.5.6-9+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets jruby.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1796.data"
# $Id: $
