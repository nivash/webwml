#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L'équipe du suivi à long terme (LTS) de Debian ne peut pas continuer
à prendre en charge divers paquets dans le cycle de vie prolongé de
Wheezy LTS. Le paquet debian-security-support fournit l'outil
check-support-status qui aide à alerter les administrateurs des paquets
installés dont le suivi de sécurité est limité ou doit prendre fin
prématurément.</p>

<p>La version 2016.05.24~deb7u1 de debian-security-support met à jour la
liste des paquets bénéficiant d'une prise en charge restreinte dans
Wheezy LTS, ajoutant les paquets suivants :</p>

<table>
<tr><th>Paquet source</th><th>Dernière version prise en charge</th><th>Date de fin de vie</th><th>Informations complémentaires</th></tr>
<tr><td>libv8</td><td>3.8.9.20-2</td><td>6 février 2016</td><td><a href="https://lists.debian.org/debian-lts/2015/08/msg00035.html">https://lists.debian.org/debian-lts/2015/08/msg00035.html</a></td></tr>
<tr><td>mediawiki</td><td>1:1.19.20+dfsg-0+deb7u3</td><td>26 avril 2016</td><td><a href="https://www.debian.org/releases/jessie/amd64/release-notes/ch-information.html#mediawiki-security">https://www.debian.org/releases/jessie/amd64/release-notes/ch-information.html#mediawiki-security</a></td></tr>
<tr><td>sogo</td><td>1.3.16-1</td><td>19 mai 2016</td><td><a href="https://lists.debian.org/debian-lts/2016/05/msg00197.html">https://lists.debian.org/debian-lts/2016/05/msg00197.html</a></td></tr>
<tr><td>vlc</td><td>2.0.3-5+deb7u2</td><td>6 février 2016</td><td><a href="https://lists.debian.org/debian-lts/2015/11/msg00049.html">https://lists.debian.org/debian-lts/2015/11/msg00049.html</a></td></tr>
</table>

<p>Si vous dépendez de ces paquets sur un système exécutant Debian 7
<q>Wheezy</q>, nous vous recommandons de mettre à niveau vers Debian 8
<q>Jessie</q>, la distribution stable actuelle. Notez néanmoins que la
prise en charge de mediawiki est aussi terminée dans Jessie.</p>

<p>Nous vous recommandons d'installer le paquet debian-security-support
pour vérifier l'état de prise en charge des paquets installés sur le
système.</p>

<p>Plus d’informations sur Debian LTS peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 2016.05.24~deb7u1 de debian-security-support </p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-487.data"
# $Id: $
