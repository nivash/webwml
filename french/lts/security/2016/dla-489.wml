#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour de sécurité corrige un problème de sécurité dans
ruby-mail. Nous vous recommandons de mettre à niveau le paquet ruby-mail.</p>

<p>Takeshi Terada (Mitsui Bussan Secure Directions, Inc.) a publié un livre
blanc intitulé <q>SMTP Injection via recipient email addresses</q>
(<a href="http://www.mbsd.jp/Whitepaper/smtpi.pdf">http://www.mbsd.jp/Whitepaper/smtpi.pdf</a>).
Ce livre blanc comporte une partie traitant de la manière dont une
vulnérabilité de ce type affectait le gem Ruby <q>mail</q>
(voir section 3.1).</p>

<p>Le livre blanc contient tous les détails spécifiques, mais
essentiellement le module du gem Ruby <q>mail</q> est prédisposé à une
attaque de destinataire dans la mesure où il ne valide ni ne vérifie pas
les adresses de destinataire données. Donc, les attaques décrites dans le
chapitre 2 du livre blanc peuvent s'appliquer au gem sans aucune
modification. Le gem Ruby <q>mail</q> lui-même n'impose pas de longueur
limite aux adresses de courriel, ainsi un attaquant peut envoyer un long
message de pourriel à l'aide d'une adresse de destinataire à moins qu'il
n'y ait une limite côté application. Cette vulnérabilité n'affecte que les
applications qui n'ont pas de validation des entrées.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.4.4-2+deb7u1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-489.data"
# $Id: $
