#use wml::debian::translation-check translation="b0c92eed441f71ac55750ecb63094fb265c115a0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de CSRF et XSS permettent à des attaquants distants
de détourner l'authentification et d'exécuter des opérations roundcube sans
l'accord de l'utilisateur. Dans certains cas, cela pourrait avoir pour
conséquence la perte ou le vol de données.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9587">CVE-2014-9587</a>

<p>Plusieurs vulnérabilités de contrefaçon de requête intersite (CSRF)
permettent à des attaquants distants de détourner l'authentification de
victimes non précisées au moyen de vecteurs inconnus, liés à (1) des
opérations de carnet d'adresses ou aux greffons (2) ACL ou (3) Managesieve.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1433">CVE-2015-1433</a>

<p>Une logique de guillemets incorrecte durant le nettoyage des attributs
de style HTML permet à des attaquants distants d'exécuter du code
JavaScript arbitraire sur le navigateur de l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4069">CVE-2016-4069</a>

<p>Une vulnérabilité de contrefaçon de requête intersite (CSRF) permet
à des attaquants distants de détourner l'authentification d'utilisateurs pour
des requêtes qui téléchargent des pièces jointes et causent un déni de
service (consommation d'espace disque) au moyen de vecteurs non précisés.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.7.2-9+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets roundcube.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-613.data"
# $Id: $
